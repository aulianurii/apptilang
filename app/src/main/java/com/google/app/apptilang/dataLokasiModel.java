package com.google.app.apptilang;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dataLokasiModel {
    @SerializedName("lokasi")
    @Expose
    private String lokasi;

    public dataLokasiModel(String lokasi){
        this.lokasi = lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getLokasi() {
        return lokasi;
    }
}
