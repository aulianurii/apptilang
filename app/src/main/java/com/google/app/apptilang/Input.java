package com.google.app.apptilang;

import android.app.DatePickerDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Input extends Fragment implements AdapterView.OnItemSelectedListener {

    private ArrayList<dataLokasiModel> dataLokasiModels;
    ArrayList<String> spinnerArray = new ArrayList<String>();
    final DatePickerDialog[] datePickerDialog = new DatePickerDialog[1];
    SimpleDateFormat dateFormatter;
    TextView tTanggal;
    Button btglTilang;
    EditText noSim;
    EditText Nama;
    EditText Plat;
    EditText Pelanggaran;
    EditText LokasiSidang;
    EditText NamaPolisi;
    Button Register, tglTilang;

    private interfaceAPI apiService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_input, container, false);

    }

    Spinner LokasiTilang;
//    Spinner spinner;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Nama = (EditText) getView().findViewById(R.id.eNama);
        Plat = (EditText) getView().findViewById(R.id.ePlat);
        noSim = (EditText) getView().findViewById(R.id.eSIM);
        LokasiTilang = (Spinner) getView().findViewById(R.id.sLokasiTilang);
        //  spinner = (Spinner) getView().findViewById(R.id.spinner);
        tglTilang = (Button) getView().findViewById(R.id.btglTilang);
        Pelanggaran = (EditText) getView().findViewById(R.id.ePelanggaran);
        LokasiSidang = (EditText) getView().findViewById(R.id.eLokasiSidang);
        NamaPolisi = (EditText) getView().findViewById(R.id.eNamaPolisi);
        Register = (Button) getView().findViewById(R.id.bRegister);


        //Date Picker

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        tTanggal = (TextView) getView().findViewById(R.id.tTanggal);
        btglTilang = (Button) getView().findViewById(R.id.btglTilang);
        btglTilang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                /**
                 * Initiate DatePicker dialog
                 */
                datePickerDialog[0] = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        /**
                         * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                         */


                        /**
                         * Set Calendar untuk menampung tanggal yang dipilih
                         */

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        /**
                         * Update TextView dengan tanggal yang kita pilih
                         */
                        tTanggal.setText(dateFormatter.format(newDate.getTime()));
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                /**
                 * Tampilkan DatePicker dialog
                 */
                datePickerDialog[0].show();
            }

        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputData();
            }
        });

        getDataLokasi();

    }

    //spinner
    private void getDataLokasi() {
        interfaceAPI apiService = serviceAPI.getAPIdata(serviceAPI.BASE_URL_API).create(interfaceAPI.class);
        final Call<ResponseBody> infoDataLokasi = apiService.getLokasi();
        infoDataLokasi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String bodyString = null;
                try {
                    bodyString = response.body() != null ? response.body().string() : response.errorBody().string();
                    Log.d("TesHasilJSON", bodyString);
                    Log.d("tesSpinner", "-3");
                    JSONArray bodyJSON = new JSONArray(bodyString);

                    //isi dataLokasi;
                    for (int i = 0; i < bodyJSON.length(); i++) {
                        spinnerArray.add(bodyJSON.getJSONObject(i).getString("lokasi"));
                    }
                    Log.d("tesSpinner", "1");
                    initInfoList();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                infoDataLokasi.cancel();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("infoDataLokasi", t.toString());
            }
        });
    }

    private void initInfoList() {
        Log.d("tesSpinner", "2");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        LokasiTilang.setAdapter(adapter);
//        spinner.setAdapter(adapter);
        Log.d("tesSpinner", "3");

    }

    private void inputData() {
        final String nama = Nama.getText().toString();
        final String plat = Plat.getText().toString();
        final String pelanggaran = Pelanggaran.getText().toString();
        final String namaPolisi = NamaPolisi.getText().toString();
        final String tanggal = tTanggal.getText().toString();
        final String lokasiSidang = LokasiSidang.getText().toString();
        final String no_sim = noSim.getText().toString();
        final String lokasiTilang = LokasiTilang.getSelectedItem().toString();


        Log.d("CreateLokasi1", tanggal);

        final Map<String, RequestBody> requestFormData = new HashMap<>();
        requestFormData.put("nama", RequestBody.create(MediaType.parse("multipart/form-data"), nama));
        requestFormData.put("no_sim", RequestBody.create(MediaType.parse("multipart/form-data"),no_sim ));
        requestFormData.put("lokasi_tilang", RequestBody.create(MediaType.parse("multipart/form-data"), lokasiTilang));
        requestFormData.put("lokasi_sidang", RequestBody.create(MediaType.parse("multipart/form-data"), lokasiSidang));
        requestFormData.put("nama_polisi", RequestBody.create(MediaType.parse("multipart/form-data"), namaPolisi));
        requestFormData.put("tanggal_sidang", RequestBody.create(MediaType.parse("multipart/form-data"), tanggal));
        requestFormData.put("plat_nomor", RequestBody.create(MediaType.parse("multipart/form-data"), plat));
        requestFormData.put("pelanggaran", RequestBody.create(MediaType.parse("multipart/form-data"), pelanggaran));

        interfaceAPI apiService = serviceAPI.getAPIdata(serviceAPI.BASE_URL).create(interfaceAPI.class);
//        final Call<ResponseBody> insertTilang = apiService.createTilang(nama, plat, pelanggaran, namaPolisi, tanggal, lokasiSidang, no_sim, lokasiTilang);
        final Call<ResponseBody> insertTilang = apiService.createTilang(requestFormData);
        insertTilang.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String bodyString = response.body() != null ? response.body().string() : response.errorBody().string();
                    Log.d("CreateLokasi", call.request().url().toString());
                    Log.d("CreateLokasi", bodyString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("CreateLokasi", call.request().url().toString());
                Log.d("CreateLokasi", "gagal");
                Log.d("CreateLokasi", t.getMessage());
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}