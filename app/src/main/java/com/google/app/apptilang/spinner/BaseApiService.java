package com.google.app.apptilang.spinner;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;

public interface BaseApiService {
    @GET("public/dataLokasi")
    Call<ResponseBody> getLokasi();

}
