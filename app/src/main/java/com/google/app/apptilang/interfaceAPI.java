package com.google.app.apptilang;

import com.google.gson.JsonElement;

import java.util.Date;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface interfaceAPI {

    @GET("public/api/infoTilang")
    Call<ResponseBody> getDaftar();

    //read data lokasi
    @GET("public/dataLokasi")
    Call<ResponseBody> getLokasi();

    //create pelanggaran tilang
//    @FormUrlEncoded
//    @POST("public/api/infoTilang/create")
//    Call<ResponseBody> createTilang(
//            @Field("nama") String nama,
//            @Field("no_sim") String no_sim,
//            @Field("lokasi_tilang") String lokasi_tilang,
//            @Field("lokasi_sidang") String lokasi_sidang,
//            @Field("nama_polisi") String nama_polisi,
//            @Field("tanggal_sidang") String tanggal_sidang,
//            @Field("plat_nomor") String plat_nomor,
//            @Field("pelanggaran") String pelanggaran
//            );

    @Multipart
    @POST("public/api/infoTilang/create")
    Call<ResponseBody> createTilang(@PartMap Map<String, RequestBody> params);
}
