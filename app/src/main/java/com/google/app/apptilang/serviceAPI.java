package com.google.app.apptilang;

import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class serviceAPI {
    private static Retrofit retrofit;
    public static final String BASE_URL = "http://info-tilang.herokuapp.com/";
    public static final String BASE_URL_API = "http://aplikasitilang.herokuapp.com/";


    public static Retrofit getAPIdata() {
        if (retrofit == null ){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

    public static Retrofit getAPIdata(String url) {
        if (retrofit == null || !url.equals(retrofit.baseUrl().toString())){
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }else
            Log.d("CreateLokasi", retrofit.baseUrl().toString());
        return retrofit;
    }
}
