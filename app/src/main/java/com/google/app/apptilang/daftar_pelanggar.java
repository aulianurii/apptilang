package com.google.app.apptilang;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class daftar_pelanggar {

    @SerializedName("nama")
    @Expose
    private String Nama;

    @SerializedName("no_sim")
    @Expose
    private String SIM;

    @SerializedName("plat_nomor")
    @Expose
    private String Plat;

    @SerializedName("lokasi_tilang")
    @Expose
    private String LokasiTilang;

    @SerializedName("pelanggaran")
    @Expose
   private String Pelanggaran;

    @SerializedName("lokasi_sidang")
    @Expose
    private String LokasiSidang;

    @SerializedName("tanggal_sidang")
    @Expose
    private String tanggalSidang;


    @SerializedName("nama_polisi")
    @Expose
    private String NamaPolisi;

    public daftar_pelanggar(String Nama, String SIM, String Plat, String LokasiTilang, String Pelanggaran, String LokasiSidang, String tanggalSidang, String NamaPolisi){
        this.Nama = Nama;
        this.SIM = SIM;
        this.Plat = Plat;
        this.LokasiTilang = LokasiTilang;
        this.Pelanggaran = Pelanggaran;
        this.LokasiSidang = LokasiSidang;
        this.tanggalSidang = tanggalSidang;
        this.NamaPolisi = NamaPolisi;
    }


    public String getTanggalSidang() {
        return tanggalSidang;
    }

    public void setTanggalSidang(String tanggalSidang) {
        this.tanggalSidang = tanggalSidang;
    }

    public String getSIM() {
        return SIM;
    }

    public void setSIM(String SIM) {
        this.SIM = SIM;
    }


    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getPlat() {
        return Plat;
    }

    public void setPlat(String plat) {
        Plat = plat;
    }

    public String getLokasiTilang() {
        return LokasiTilang;
    }

    public void setLokasiTilang(String lokasiTilang) {
        LokasiTilang = lokasiTilang;
    }

    public String getPelanggaran() {
        return Pelanggaran;
    }

    public void setPelanggaran(String pelanggaran) {
        Pelanggaran = pelanggaran;
    }

    public String getLokasiSidang() {
        return LokasiSidang;
    }

    public void setLokasiSidang(String lokasiSidang) {
        LokasiSidang = lokasiSidang;
    }

    public String getNamaPolisi() {
        return NamaPolisi;
    }

    public void setNamaPolisi(String namaPolisi) {
        NamaPolisi = namaPolisi;
    }


}
