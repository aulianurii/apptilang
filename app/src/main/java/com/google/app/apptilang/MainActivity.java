package com.google.app.apptilang;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        BottomNavigationView bottomnav = findViewById(R.id.botnav);
        bottomnav.setOnNavigationItemSelectedListener(navlistener);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navlistener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {
                        case R.id.iInput:
                            selectedFragment = new Input();
                            break;
                        case R.id.iList:
                            selectedFragment = new Daftar_Pelanggaran();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mainactivity,
                            selectedFragment).commit();

                    return true;
                }
            };


}
