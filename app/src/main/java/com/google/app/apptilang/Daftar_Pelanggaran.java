package com.google.app.apptilang;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Daftar_Pelanggaran extends Fragment {

    private adapter_daftar_pelanggar adapter_daftar_pelanggar;
    private GridLayoutManager gridLayoutManager;

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.LayoutManager layoutManager;
    private adapter_daftar_pelanggar adapter;
    private RecyclerView rv_data;
    private ArrayList<daftar_pelanggar> daftar_pelanggar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daftar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv_data = getView().findViewById(R.id.rv_data);
        getDaftar();
    }

    private void getDaftar() {
        interfaceAPI interfaceAPI = serviceAPI.getAPIdata().create(interfaceAPI.class);
        final Call<ResponseBody> getDaftar = interfaceAPI.getDaftar();
        getDaftar.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String bodyString = null;
                try {
                    bodyString = response.body() != null ? response.body().string() : response.errorBody().string();
                    Log.d("TesHasilJSON", bodyString);
                    JSONObject bodyJSON = new JSONObject(bodyString);
                    daftar_pelanggar = new ArrayList<>();
                    JSONArray result = bodyJSON.getJSONArray("result");

                    //isi InfoTilang;
                    for (int i = 0; i < result.length(); i++) {
//                            (String Nama, String SIM, String Plat, String LokasiTilang, String Pelanggaran, String LokasiSidang, String tanggalSidang, String NamaPolisi
                        JSONObject dataResult = result.getJSONObject(i);
                        daftar_pelanggar.add(new daftar_pelanggar(dataResult.getString("nama"), dataResult.getString("no_sim"), dataResult.getString("plat_nomor"),
                                dataResult.getString("lokasi_tilang"), dataResult.getString("pelanggaran"), dataResult.getString("lokasi_sidang"),
                                dataResult.getString("tanggal_sidang"), dataResult.getString("nama_polisi")));
                    }
                    initdaftar_pelanggar();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("infoDataTilang", t.toString());
            }
        });
    }

    private void initdaftar_pelanggar() {
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        linearLayoutManager = new LinearLayoutManager(getContext());
        adapter_daftar_pelanggar = new adapter_daftar_pelanggar(daftar_pelanggar);
        rv_data.setLayoutManager(gridLayoutManager);
        rv_data.setHasFixedSize(true);
        rv_data.setAdapter(adapter_daftar_pelanggar);
    }
}