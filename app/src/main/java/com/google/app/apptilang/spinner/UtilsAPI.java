package com.google.app.apptilang.spinner;

public class UtilsAPI {

    public static final String BASE_URL_API = "http://aplikasitilang.herokuapp.com/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService() {
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}

