package com.google.app.apptilang;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class adapter_daftar_pelanggar extends RecyclerView.Adapter<adapter_daftar_pelanggar.ViewHolder> {

    public ArrayList<daftar_pelanggar> daftar_pelanggar;

    public static class ViewHolder extends RecyclerView.ViewHolder{


        public TextView nama, no_sim, plat_nomor, lokasi_tilang, pelanggaran, lokasi_sidang, nama_polisi ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.tv_nama);
            no_sim = itemView.findViewById(R.id.tv_sim);
            plat_nomor = itemView.findViewById(R.id.tv_plat);
            lokasi_tilang = itemView.findViewById(R.id.tv_lokasi_tilang);
            pelanggaran = itemView.findViewById(R.id.tv_pelanggaran);
            lokasi_sidang = itemView.findViewById(R.id.tv_lokasi_sidang);
            nama_polisi = itemView.findViewById(R.id.tv_napol);
        }
    }

    public adapter_daftar_pelanggar (ArrayList <daftar_pelanggar> daftar_pelanggar){
        this.daftar_pelanggar = daftar_pelanggar ;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nama.setText(daftar_pelanggar.get(position).getNama());
        holder.no_sim.setText(daftar_pelanggar.get(position).getSIM());
        holder.plat_nomor.setText(daftar_pelanggar.get(position).getPlat());
        holder.lokasi_tilang.setText(daftar_pelanggar.get(position).getLokasiTilang());
        holder.pelanggaran.setText(daftar_pelanggar.get(position).getPelanggaran());
        holder.lokasi_sidang.setText(daftar_pelanggar.get(position).getLokasiSidang());
        holder.nama_polisi.setText(daftar_pelanggar.get(position).getNamaPolisi());
    }

    @Override
    public int getItemCount() {
        return daftar_pelanggar.size();
    }


}
